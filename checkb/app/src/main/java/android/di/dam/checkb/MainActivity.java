package android.di.dam.checkb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final CheckBox myCheck = (CheckBox) findViewById(R.id.checkBox);

        myCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myCheck.isChecked()){
                    myCheck.setText("Ha sido marcado");
                }else{
                    myCheck.setText("Ha sido desmarcado");
                }
            }
        });
    }
}
