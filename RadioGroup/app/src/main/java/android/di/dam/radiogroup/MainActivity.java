package android.di.dam.radiogroup;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView txtResultado = (TextView) findViewById(R.id.txtResultado);

        /*final RadioButton rdbtnAd= (RadioButton) findViewById(R.id.opAd);
        final RadioButton rdbtnPrg= (RadioButton) findViewById(R.id.opPrg);
        final RadioButton rdbtnDi= (RadioButton) findViewById(R.id.opDI);

        final View.OnClickListener radioListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final RadioButton rdbtSelected = (RadioButton) v;
                if(rdbtSelected.getId()==R.id.opDI){
                    txtResultado.setText("Bien elegido");
                    txtResultado.setTextColor(Color.GREEN);
                }else{
                    txtResultado.setText("¿Estás seguro?");
                    txtResultado.setTextColor(Color.RED);

                }
            }
        };

        rdbtnAd.setOnClickListener(radioListener);
        rdbtnPrg.setOnClickListener(radioListener);
        rdbtnDi.setOnClickListener(radioListener);*/

       RadioGroup radioGroup = (RadioGroup) findViewById(R.id.rdgrp);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
               // Log.d("General","Seleccionas: " + checkedId);
                if(checkedId==R.id.opDI){
                    txtResultado.setText("Bien elegido");
                    txtResultado.setTextColor(Color.GREEN);
                }else{
                    txtResultado.setText("¿Estás seguro?");
                    txtResultado.setTextColor(Color.RED);

                }
            }
        });
    }
}
